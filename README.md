# SummerCamp Homework

这是[智能系统实验室暑期培训](https://gitee.com/pi-lab/SummerCamp/tree/master/)的作业。该课程作业包括工具类（Linux，Git），语言（C++，Python），计算机视觉（多视图几何，SLAM），机器学习（传统方法、深度学习）等多方面的练习题。

有志于从事计算机视觉、机器学习方面的同学，认真完成练习题目，在完成之后深入思考如何优化代码等，从而有效提高自己分析问题、分解问题、编程、Debug等多方面的能力，为后续研究生学习打好坚实基础。


##  Homework List

| Topic | Description |
| :---: | :---------: |
| [tool/linux]| 了解常用命令，练习Bash编程 |
| [tool/git]| 了解github常用操作，并练习协作编辑仓库 |
| | |
| [cpp/helloc]| 回顾c语言 |
| [python/hellopython]| python入门与基础回顾 |
| [cpp/hellocpp]| class,继承多态练习 |
| [python/more]| python流程处理，类对象编程 |
| [cpp/stl]| 实现模板vector |
| [python/module]| python包,class编写，练习画图 |
| [tool/markdown]| 编写Markdown说明书 |
| [tool/cmake]| 编写CMakeLists.txt |
| [cpp/cpp11]| 多线程求和 |
| [cpp/style]| 使用cpplint统一GSLAM编程风格 |
| | |
| [slam/geo2d3d]| 点线面变换多视图基础知识回顾 |
| [slam/match]| 特征点匹配练习 |
| [slam/camera]| 相机模型及标定练习 |
| [slam/liegroup]| 刚体运动及其变换 |
| [slam/opt_linear]| 线性优化及模型鲁棒估计 |
| [slam/initialize]| 单目初始化 |
| [slam/opt_nolinear]| 使用LM算法实现PnP |
| [slam/tracking]| 实现一个简单SLAM插件 |
| | |
| [ai/rg_lg]| 线性回归以及逻辑斯特回归 |
| [ai/nn]| 多层网络 |
| [ai/cnn]| 卷及神经网络 |
| [ai/unsupervised_learning]| 无监督学习 |
| [ai/gans]| 生成对抗网络 |



[tool/linux]: ./tool/linux/README.md
[tool/git]: ./tool/git/README.md
[tool/markdown]: ./tool/markdown/README.md
[tool/cmake]: ./tool/cmake/README.md
[python/hellopython]: ./python/hellopython/README.md
[python/more]: ./python/more/README.md
[python/module]: ./python/module/README.md
[cpp/helloc]: ./cpp/helloc/README.md
[cpp/hellocpp]: ./cpp/hellocpp/README.md
[cpp/stl]: ./cpp/stl/README.md
[cpp/cpp11]: ./cpp/cpp11/README.md
[cpp/style]: ./cpp/style/README.md
[slam/geo2d3d]: ./slam/geo2d3d/README.md
[slam/match]: ./slam/match/README.md
[slam/camera]: ./slam/camera/README.md
[slam/liegroup]: ./slam/liegroup/README.md
[slam/opt_linear]: ./slam/opt_linear/README.md
[slam/initialize]: ./slam/initialize/README.md
[slam/opt_nolinear]: ./slam/opt_nolinear/README.md
[slam/tracking]: ./slam/tracking/README.md
[ai/rg_lg]: ./ai/rg_lg/README.md
[ai/nn]: ./ai/nn/README.md
[ai/cnn]: ./ai/cnn/README.md
[ai/unsupervised_learning]: ./ai/unsupervised_learning/README.md
[ai/gans]: ./ai/gans/README.md


